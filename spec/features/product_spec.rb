# encoding: utf-8
require 'spec_helper'

describe "Visiting Products", inaccessible: true do
  let!(:product_1) do
    create(:product, name: "Ruby on Rails Ringer T-Shirt",
           price: 19.99,
           sale_price: 12.99)
  end

  let!(:product_2) do
    create(:product, name: "Ruby on Rails Ringer T-Shirt",
           price: 21.99)
  end

  context "on products#index page" do
    it "show the correct price" do
      visit spree.products_path
      within "#product_#{product_1.id} .price" do
        expect(page).to have_content("12.99")
      end
      within "#product_#{product_2.id} .price" do
        expect(page).to have_content("21.99")
      end
    end
  end

  context "on product page" do
    context "whith sale price" do
      it "show the sale price for a product" do
        visit spree.product_path(product_1)
        within ".price" do
          expect(page).to have_content("12.99")
        end
      end
    end
  end

  context "without sale price" do
    it "show the original price" do
      visit spree.product_path(product_2)
      within ".price" do
        expect(page).to have_content("21.99")
      end
    end
  end
end

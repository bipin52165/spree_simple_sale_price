# encoding: utf-8

require 'spec_helper'

describe Spree::Product do
  let!(:product) { create(:product, price: 100, sale_price: 50) }

  it { product.respond_to? :sale_price }
  it { product.respond_to? :sale_price= }
  it { product.respond_to? :original_price }
  it { product.respond_to? :on_sale? }

  it "can assign sale price to price" do
    expect( product.master.default_price.sale_price ).to eq(50)
  end

  it "#price returns the sale price if exists" do
    expect(product.price).to eq(50)
  end

  it "#original_price returs the normal price" do
    expect(product.original_price).to eq(100)
  end

  context "#on_sale?" do
    it "is true if sale price exists" do
      expect(product.on_sale?).to be_true
    end

    it "is false if sale price does not exist" do
      product.sale_price = nil
      expect(product.on_sale?).to be_false
    end
  end
end


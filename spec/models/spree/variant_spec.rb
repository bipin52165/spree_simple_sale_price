# encoding: utf-8

require 'spec_helper'

describe Spree::Variant do
  let!(:variant) { create(:variant, price: 100, sale_price: 50) }

  it { variant.respond_to? :price }
  it { variant.respond_to? :sale_price }
  it { variant.respond_to? :sale_price= }
  it { variant.respond_to? :original_price }
  it { variant.respond_to? :on_sale? }

  it "can assign sale price to price" do
    expect( variant.default_price.sale_price ).to eq(50)
  end

  it "#price returns the sale price if exists" do
    expect(variant.price).to eq(50)
  end

  it "#original_price returs the normal price" do
    expect(variant.original_price).to eq(100)
  end

  context "#on_sale?" do
    it "is true if sale price exists" do
      expect(variant.on_sale?).to be_true
    end

    it "is false if sale price does not exist" do
      variant.sale_price = nil
      expect(variant.on_sale?).to be_false
    end
  end
end

module Spree
  Price.class_eval do
    alias :display_original_price :display_price
    alias :display_original_amount :display_amount

    def display_amount
      money_or_sale_money
    end
    alias :display_price :display_amount

    def money_or_sale_money
      Spree::Money.new(sale_price_amount || amount || 0, {currency: currency})
    end

    def amount
      original_amount
    end
    alias :price :amount

    def original_amount
      self[:amount]
    end
    alias :original_price :original_amount

    def sale_price_t
      sale_price_amount
    end

    def on_sale?
      sale_price_amount.present?
    end

    def sale_price_t=(price)
      self[:sale_price_amount] = parse_price(price)
    end
  end
end

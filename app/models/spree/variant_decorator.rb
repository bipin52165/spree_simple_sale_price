module Spree
  Variant.class_eval do
    delegate_belongs_to :default_price, :sale_price, :sale_price=,
      :original_price, :on_sale?, :currency
  end
end


class AddSalePriceAmountToSpreePrices < ActiveRecord::Migration
  def change
    add_column :spree_prices, :sale_price_amount, :decimal, precision: 8, scale: 2
  end
end
